package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ribomation.domain.PersonCsvMapper;
import ribomation.domain.PersonLoader;

import static org.junit.jupiter.api.Assertions.*;

class PersonLoaderTest {
    PersonLoader target;

    @BeforeEach
    void setUp() {
        target = new PersonLoader();
        target.setMapper(new PersonCsvMapper());
    }

    @Test
    void load() {
        var file = "/persons.csv";
        var is = getClass().getResourceAsStream(file);
        assertNotNull(is);

        final int[] cnt = {0};
        target.load(is, p -> ++cnt[0]);
        assertEquals(3000, cnt[0]);
    }

}
