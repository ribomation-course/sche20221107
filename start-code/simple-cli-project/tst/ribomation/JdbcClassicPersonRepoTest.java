package ribomation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ribomation.domain.Person;
import ribomation.jdbc.H2DataSourceBuilder;
import ribomation.jdbc.JdbcClassicPersonRepo;
import ribomation.jdbc.PersonJdbcMapper;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class JdbcClassicPersonRepoTest {
    JdbcClassicPersonRepo target;

    @BeforeEach
    void setUp() throws SQLException {
        target = new JdbcClassicPersonRepo();
        target.setMapper(new PersonJdbcMapper());
    }

    @AfterEach
    void tearDown() {
        target.close();
    }

    private void init() {
        target.setDataSource(new H2DataSourceBuilder().openMemory());
        target.createTable();
    }

    private void insertThree() {
        target.insert(new Person("Anna", 23, true, 11111));
        target.insert(new Person("Berit", 33, true, 22222));
        target.insert(new Person("Carin", 43, true, 33333));
    }

    @Test
    void createTable() {
        init();
    }

    @Test
    void insert() {
        init();
        var p = new Person("Nisse", 42, false, 12345);
        var id = target.insert(p);
        assertEquals(1, id);
    }

    @Test
    void countAll() {
        init();
        insertThree();
        var cnt = target.countAll();
        assertEquals(3, cnt);
    }

    @Test
    void findAll() {
        init();
        insertThree();
        var all = target.findAll();
        assertEquals(3, all.size());
        assertEquals("Anna", all.get(0).getName());
    }


    @Test
    void findById() {
        init();
        insertThree();
        var p = target.findById(3);
        assertTrue(p.isPresent());
        assertEquals("Carin", p.get().getName());
    }

    @Test
    void update() {
        init();
        insertThree();

        var id = 1;
        assertEquals("Anna", target.findById(id).orElseThrow().getName());

        target.update(id, new Person("Nisse", 0, false, 0));
        assertEquals("Nisse", target.findById(id).orElseThrow().getName());
    }

    @Test
    void delete() {
        init();
        insertThree();

        assertEquals(3, target.countAll());
        target.delete(2);
        assertEquals(2, target.countAll());
    }

    @Test
    void findByAgeBetweenAndPostCodeLessThanAndFemale() {
        init();
        target.insert(new Person("Anna", 23, true, 10_095));
        target.insert(new Person("Berit", 33, true, 10_195));
        target.insert(new Person("Carl", 35, false, 10_095));
        target.insert(new Person("Doris", 37, true, 10_095));
        target.insert(new Person("Eva", 39, true, 10_095));
        target.insert(new Person("Frida", 43, true, 10_095));

        var ageLower = 30;
        var ageUpper = 40;
        var postCodeUpper = 10_100;

        var lst = target.findByAgeBetweenAndPostCodeLessThanAndFemale(ageLower, ageUpper, postCodeUpper);
        assertEquals(2, lst.size());
        assertEquals("Doris", lst.get(0).getName());
        assertEquals("Eva", lst.get(1).getName());
    }
}