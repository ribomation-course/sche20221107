import httpClient from 'axios'

const url = 'http://localhost:8080/api/persons'


export async function findAll() {
    const response = await httpClient.get(url)
    if (response.status === 200) {
        return response.data;
    } else {
        console.error('failed: %d %s', response.status, response.statusText)
        return []
    }
}

export async function findById(id) {
    const response = await httpClient.get(`${url}/${id}`)
    if (response.status === 200) {
        return response.data
    } else {
        console.error('failed: %d %s', response.status, response.statusText)
        return {}
    }
}

export async function insert(product) {
    const response = await httpClient.post(url, product)
    if (response.status === 201) {
        return response.data
    } else {
        console.error('failed: %d %s', response.status, response.statusText)
        return {}
    }
}

export async function update(id, product) {
    const response = await httpClient.put(`${url}/${id}`, product)
    if (response.status === 200) {
        return response.data
    } else {
        console.error('failed: %d %s', response.status, response.statusText)
        return undefined
    }
}

export async function remove(id) {
    const response = await httpClient.delete(`${url}/${id}`)
    if (response.status === 204) {
        return true
    } else {
        console.error('failed: %d %s', response.status, response.statusText)
        return undefined
    }
}
