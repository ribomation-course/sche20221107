export interface Person {
    id?: number;
    name: string;
    age: number;
    email: string;
    company: string;
    avatar?: string;
}
