import {Component, OnInit} from '@angular/core';
import {PersonService} from "../../services/person.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Person} from "../../domain/person";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.page.html',
    styleUrls: ['./edit.page.css']
})
export class EditPage implements OnInit {
    person: Person | undefined;
    id: number = -1;

    constructor(
        private personSvc: PersonService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        this.loadItem(id);
    }

    async cancel() {
        await this.router.navigate(['/list'])
    }

    loadItem(id: number | string | null) {
        if (!!id) {
            this.id = Number(id);
            this.personSvc
                .findById(this.id)
                .subscribe(obj => this.person = obj);
        }
    }

    saveItem() {
        if (this.person == undefined) return;
        
        if (this.id === -1) {
            this.personSvc
                .insert(this.person)
                .subscribe(_ => this.cancel());
        } else  {
            this.personSvc
                .update(this.id, this.person)
                .subscribe(_ => this.cancel());
        }
    }

}
