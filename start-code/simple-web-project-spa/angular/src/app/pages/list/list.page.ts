import {Component, OnInit} from '@angular/core';
import {PersonService} from "../../services/person.service";
import {Person} from "../../domain/person";

@Component({
    selector: 'app-list',
    templateUrl: './list.page.html',
    styleUrls: ['./list.page.css']
})
export class ListPage implements OnInit {
    persons: Person[] = [];

    constructor(private personSvc: PersonService) {
    }

    ngOnInit(): void {
        this.load();
    }

    load() {
        this.personSvc
            .findAll()
            .subscribe(objs => this.persons = objs);
    }

    removeItem(id: number | undefined) {
        if (!!id) {
            this.personSvc
                .remove(id)
                .subscribe(() => this.load());
        }
    }
}
