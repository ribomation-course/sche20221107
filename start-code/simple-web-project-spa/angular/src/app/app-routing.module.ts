import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListPage} from "./pages/list/list.page";
import {ShowPage} from "./pages/show/show.page";
import {EditPage} from "./pages/edit/edit.page";
import {NotFoundPage} from "./pages/not-found/not-found.page";

const routes: Routes = [
    {path: 'list', component: ListPage},
    {path: 'show/:id', component: ShowPage},
    {path: 'edit/:id', component: EditPage},
    {path: '', redirectTo: '/list', pathMatch: 'full'},
    {path: '**', component: NotFoundPage}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
