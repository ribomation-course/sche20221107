package ribomation.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class Person {
    //name;age;gender;postCode
    private int id;
    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public static Person of(String name, String age, String gender, String postCode) {
        return of(-1, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

    public static Person of(int id, String name, String age, String gender, String postCode) {
        return of(id, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

    public static Person of(int id, String name, int age, String gender, int postCode) {
        return of(id, name, age, gender.equals("Female"), postCode);
    }

    public String isFemaleAsString() {
        return isFemale() ? "Female" : "Male";
    }
}
