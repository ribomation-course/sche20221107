package ribomation.web_spa.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class SpaForwardController {
    @GetMapping({"", "list", "show/*", "edit/*"})
    public String forwardToIndex() {
        return "forward:/index.html";
    }
}
