package ribomation.web_spa.web;

public class NotFound extends RuntimeException {
    final int id;

    public NotFound(int id) {
        super(String.format("cannot find item with id=%d", id));
        this.id = id;
    }
}
