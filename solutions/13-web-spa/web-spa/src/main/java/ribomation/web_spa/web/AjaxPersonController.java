package ribomation.web_spa.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.web_spa.domain.Person;
import ribomation.web_spa.domain.PersonDAO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/persons")
@CrossOrigin
public class AjaxPersonController {
    @Autowired
    private PersonDAO dao;

    @GetMapping
    public List<Person> list() {
        return dao.findAll();
    }

    @GetMapping("/{id}")
    public Person show(@PathVariable int id) {
        if (id == -1) {
            return dao.create();
        } else {
            return lookup(id);
        }
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable int id, @RequestBody Person delta) {
        dao.update(id, delta);
        return lookup(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        lookup(id);
        dao.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person save(@RequestBody Person delta) {
        var p = dao.create();
        dao.insert(p);
        final var id = p.getId();
        dao.update(id, delta);
        return lookup(id);
    }

    @ExceptionHandler(NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> notFound(NotFound x) {
        return Map.of(
                "message", x.getMessage(),
                "id", Integer.toString(x.id)
        );
    }

    private Person lookup(int id) {
        return dao.findById(id).orElseThrow(() -> new NotFound(id));
    }
}
