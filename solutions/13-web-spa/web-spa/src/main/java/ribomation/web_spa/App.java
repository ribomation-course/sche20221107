package ribomation.web_spa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ribomation.web_spa.domain.PersonDAO;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    CommandLineRunner populate(PersonDAO dao) {
        return args -> {
            dao.populate(6);
            System.out.printf("populated %d objs%n", dao.count());
        };
    }
}
