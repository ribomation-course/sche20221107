package ribomation.lombok_demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.InputStream;

@SpringBootApplication
public class LombokDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(LombokDemoApplication.class, args);
    }

    @Bean
    CommandLineRunner doit(PersonLoader loader, InputStream csvResource) {
        return args -> {
            loader.loadAll(csvResource).stream()
                    .limit(10)
                    .forEach(System.out::println);
        };
    }

    @Bean
    InputStream csvResource() {
        return getClass().getResourceAsStream("/persons.csv");
    }
}
