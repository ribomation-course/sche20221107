package ribomation.lombok_demo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class Person {
    //name;age;gender;postCode
    private int id;
    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public static Person of(int id, String name, String age, String gender, String postCode) {
        return of(id, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }
}
