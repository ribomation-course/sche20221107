package ribomation.jdbc;


import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ribomation.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class SpringPersonJdbcMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            var id = rs.getInt("id");
            var name = rs.getString("name");
            var age = rs.getInt("age");
            var gender = rs.getString("gender");
            var postCode = rs.getInt("postCode");
            return Person.of(id, name, age, gender, postCode);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
