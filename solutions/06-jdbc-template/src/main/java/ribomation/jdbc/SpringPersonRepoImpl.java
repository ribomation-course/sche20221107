package ribomation.jdbc;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ribomation.domain.Person;
import ribomation.domain.PersonRepo;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository("springDao")
public class SpringPersonRepoImpl implements PersonRepo, InitializingBean {
    private final JdbcTemplate jdbc;
    private final RowMapper<Person> rowMapper;

    @Autowired
    public SpringPersonRepoImpl(DataSource ds, RowMapper<Person> rowMapper) {
        jdbc = new JdbcTemplate(ds);
        this.rowMapper = rowMapper;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    private void createTable() {
        final String tableSQL = """
                DROP TABLE IF EXISTS persons;
                CREATE TABLE persons (
                    id          int primary key auto_increment,
                    name        varchar(64),
                    age         int,
                    gender      varchar(6),
                    postCode    int
                );
                """;
        jdbc.execute(tableSQL);
    }

    @Override
    public int countAll() {
        String sql = "SELECT count(*) FROM " + TABLE_NAME;
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Person> findAll() {
        String sql = "SELECT * FROM " + TABLE_NAME;
        return jdbc.query(sql, rowMapper);
    }

    @Override
    public Optional<Person> findById(int id) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            return Optional.ofNullable(jdbc.queryForObject(sql, rowMapper, id));
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper) {
        var sql = "SELECT * FROM " + TABLE_NAME + " WHERE age BETWEEN ? AND ? AND postCode <= ? AND gender = 'Female'";
        return jdbc.query(sql, rowMapper, ageLower, ageUpper, postCodeUpper);
    }

    @Override
    public int insert(Person p) {
        var sql = "INSERT INTO " + TABLE_NAME + " (name,age,gender,postCode) VALUES (?,?,?,?)";
        var cnt = jdbc.update(sql, p.getName(), p.getAge(), p.isFemaleAsString(), p.getPostCode());
        if (cnt != 1) {
            throw new RuntimeException("insert failed");
        }
        return 0;
    }

    @Override
    public void insert(List<Person> personCollection) {
        final var sql = "insert into " + TABLE_NAME + " (name,age,gender,postcode) values (?,?,?,?)";
        final var batchSize = 1000;
        jdbc.batchUpdate(sql, personCollection, batchSize, (ps, p) -> {
            ps.setString(1, p.getName());
            ps.setInt(2, p.getAge());
            ps.setString(3, p.isFemaleAsString());
            ps.setInt(4, p.getPostCode());
        });
    }

    @Override
    public void update(int id, Person p) {
        var sql = "UPDATE " + TABLE_NAME + " SET name=?, age=?, gender=?, postCode=? WHERE id=?";
        jdbc.update(sql, p.getName(), p.getAge(), p.isFemaleAsString(), p.getPostCode(), id);
    }

    @Override
    public void delete(int id) {
        var sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        jdbc.update(sql, id);
    }
}
