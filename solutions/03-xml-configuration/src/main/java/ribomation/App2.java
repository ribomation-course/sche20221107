package ribomation;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;

import java.io.IOException;

public class App2 {
    public static void main(String[] args) throws Exception {
        var app = new App2();
        app.runXML();
    }

    void runXML() throws IOException {
        var ctx         = new ClassPathXmlApplicationContext("/beans2.xml");

        var csvResource = ctx.getResource("classpath:/persons.csv").getInputStream();
        var jsonMapper  = ctx.getBean("jsonMapper", PersonJsonMapper.class);
        var loader      = ctx.getBean("loader", PersonLoader.class);
        var dao         = ctx.getBean("dao", PersonRepo.class);

        var useCase     = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }

}
