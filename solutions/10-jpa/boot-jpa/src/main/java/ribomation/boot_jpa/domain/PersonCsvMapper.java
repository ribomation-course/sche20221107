package ribomation.boot_jpa.domain;

import org.springframework.stereotype.Service;

@Service
public class PersonCsvMapper {
    public String defaultDelim = ";";

    public PersonCsvMapper() {
    }

    public PersonCsvMapper(String defaultDelim) {
        this.defaultDelim = defaultDelim;
    }

    public Person fromCSV(String csv) {
        return fromCSV(csv, defaultDelim);
    }

    public Person fromCSV(String csv, String delim) {
        var f = csv.split(delim);
        var ix = 0;

        //name;age;gender;postCode
        var name = f[ix++];
        var age = f[ix++];
        var gender = f[ix++];
        var postCode = f[ix++];

        try {
            return Person.create(name, age, gender, postCode);
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }
    }
}
