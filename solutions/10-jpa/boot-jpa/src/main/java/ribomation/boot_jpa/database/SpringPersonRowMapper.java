package ribomation.boot_jpa.database;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ribomation.boot_jpa.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class SpringPersonRowMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        var id = rs.getInt("id");
        var name = rs.getString("name");
        var age = rs.getInt("age");
        var female = rs.getString("gender").equalsIgnoreCase("female");
        var postCode = rs.getInt("postCode");

        return Person.create(id, name, age, female, postCode);
    }
}
