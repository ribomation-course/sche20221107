package ribomation.boot_jpa;

import org.springframework.stereotype.Service;
import ribomation.boot_jpa.database.PersonDAO;
import ribomation.boot_jpa.domain.PersonJsonMapper;
import ribomation.boot_jpa.domain.PersonLoader;

import java.io.InputStream;

@Service
public class UseCase {
    InputStream csvResource;
    PersonLoader loader;
    PersonDAO dao;
    PersonJsonMapper jsonMapper;

    public UseCase(InputStream csvResource, PersonLoader loader, PersonDAO dao, PersonJsonMapper jsonMapper) {
        this.csvResource = csvResource;
        this.loader = loader;
        this.dao = dao;
        this.jsonMapper = jsonMapper;
    }

    public void run() {
        var objs = loader.loadAll(csvResource);
        System.out.printf("Loaded %d rows from CSV%n", objs.size());

        dao.saveAll(objs);
        System.out.printf("Inserted %d persons%n", dao.count());

        var selection = dao.findByAgeBetweenAndPostCodeLessThanAndFemale(30, 40, 12_500, true);
        System.out.printf("Found %d persons%n", selection.size());
        System.out.printf("JSON: %s%n", jsonMapper.toJson(selection));

        dao.deleteAll(selection);
        System.out.printf("Removed %d persons%n", selection.size());
        System.out.printf("Reduced size to %d persons%n", dao.count());

        var onePerson = dao.findById(1).orElseThrow();
        System.out.printf("Found: %s%n", onePerson);

        onePerson.setName("Per Silja");
        onePerson.setAge(42);
        onePerson.setFemale(false);
        onePerson.setPostCode(25_500);

        dao.save(onePerson);
        System.out.printf("Modified: %s%n", dao.findById(1).orElseThrow());
    }
}
