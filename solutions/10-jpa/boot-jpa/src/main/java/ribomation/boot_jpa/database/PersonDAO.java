package ribomation.boot_jpa.database;

import org.springframework.data.repository.CrudRepository;
import ribomation.boot_jpa.domain.Person;

import java.util.List;

public interface PersonDAO extends CrudRepository<Person, Integer> {

   List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int lbAge, int ubAge, int postCode, boolean female);

}
