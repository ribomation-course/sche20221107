package ribomation.boot_jpa.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor
@Entity
public class Person {
    @Id @GeneratedValue
    private int id;

    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public static Person create(String name, String age, String gender, String postCode) {
        return create(-1, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

    public static Person create(String name, int age, boolean female, int postCode) {
        return create(-1, name, age, female, postCode);
    }

}
