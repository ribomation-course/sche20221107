package ribomation.boot_hello_gradle;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BootHelloGradleApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootHelloGradleApplication.class, args);
    }

    @Bean
    CommandLineRunner doit() {
        return args -> {
            System.out.println("Hello from a Spring Boot app");
        };
    }
}
