package ribomation.boot_hello_maven;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BootHelloMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootHelloMavenApplication.class, args);
    }
    @Bean
    CommandLineRunner doit() {
        return args -> {
            System.out.println("Hello from a Spring Boot app, using Maven");
        };
    }
}
