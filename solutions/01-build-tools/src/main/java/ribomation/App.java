package ribomation;

import com.github.ricksbrown.cowsay.plugin.CowExecutor;

public class App {
    public static void main(String[] args) {
        var exe = new CowExecutor();
        exe.setCowfile("stegosaurus");
        exe.setMessage("Hello from a Java app, built with a tool");
        System.out.println(exe.execute());
    }
}
