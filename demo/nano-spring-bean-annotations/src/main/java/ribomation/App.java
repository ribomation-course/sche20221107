package ribomation;

import ribomation.domain.Car;
import ribomation.domain.Cat;
import ribomation.domain.Engine;
import ribomation.domain.Person;
import ribomation.nano_spring.BeanRepository;

public class App {
    public static void main(String[] args) {
        try {
            App app = new App();
            app.init();
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BeanRepository beans;

    void init() {
        beans = BeanRepository.createFromPackage("ribomation.domain");
        dumpDefs();
    }

    void run() {
        var obj = beans.getBeanByType("ribomation.domain.Engine", Engine.class);
        System.out.printf("(1) obj: %s%n", obj);
        dumpBeans();

        System.out.println("----");
        var obj2 = beans.getBean("haskel", Cat.class);
        System.out.printf("(2) obj: %s%n", obj2);
        dumpBeans();

        System.out.println("----");
        var obj3 = beans.getBeanByType("ribomation.domain.Car", Car.class);
        System.out.printf("(3) obj: %s%n", obj3);
        dumpBeans();

        System.out.println("----");
        var obj4 = beans.getBean("nisse", Person.class);
        System.out.printf("(4) obj: %s%n", obj4);
        dumpBeans();
    }

    void dumpBeans() {
        System.out.println("-- Defined Beans --");
        beans.getBeans().forEach((name, bean) -> {
            System.out.printf("%s) %s%n", name, bean);
        });
    }

    void dumpDefs() {
        System.out.println("-- Bean Classes --");
        beans.getDefinitions().forEach((clsName, cls) -> {
            System.out.printf("%s) %s%n", clsName, cls.getName());
        });
    }

}
