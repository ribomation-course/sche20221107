package ribomation.domain;


import ribomation.nano_spring.Bean;
import ribomation.nano_spring.Inject;

import java.util.Objects;
@Bean("nisse")
public class Person {
    private String name;
    private @Inject Car car;
    private @Inject("haskel") Cat cat;

    public Person() {
        var ann = getClass().getAnnotation(Bean.class);
        if (!ann.value().isBlank()) {
            this.name = ann.value();
        }
    }

    public Person(String name, Car car, Cat cat) {
        this.name = name;
        this.car = car;
        this.cat = cat;
    }

    @Override
    public String toString() {
        return String.format("Person{%s, my car: %s, my cat: %s}",
                name, car, cat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(car, person.car) &&
                Objects.equals(cat, person.cat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, car, cat);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
}
