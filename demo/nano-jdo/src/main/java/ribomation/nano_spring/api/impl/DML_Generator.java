package ribomation.nano_spring.api.impl;

public class DML_Generator extends GeneratorSupport {
    public String generateInsert(Class domainClass) {
        var tableName   = getTableName(domainClass);
        var columnNames = getColumnNames(domainClass, false);
        return String.format("INSERT INTO %s (%s) VALUES (%s)",
                tableName,
                toNames(columnNames),
                toQuestions(columnNames)
        );
    }

    public String generateDelete(Class domainClass) {
        var tableName = getTableName(domainClass);
        var pk        = getPrimaryKey(domainClass);
        return String.format("DELETE FROM %s WHERE %s = ?", tableName, pk);
    }

    public String generateUpdate(Class domainClass) {
        throw new UnsupportedOperationException("generateUPDATE");
    }
}

