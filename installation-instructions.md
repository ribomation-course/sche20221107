# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download
* Eclipse
    - https://www.eclipse.org/ide/
* Something else, you already are familiar with

## Java JDK
The decent version of Java JDK installed, appropriate for Spring, such as v19,
or at least v16.
* [Java JDK Download](https://jdk.java.net/19/)

## Access to Spring Initializr Web
Ability to configure and download a project ZIP from `https://start.spring.io/`


## Build Tool
Need to have one of the build tools Maven or Gradle installed. However,
when generating a project via Spring Initializr, there is a wrapper shell
script to use.

* Gradle: `gradlew` / `gradle.bat`
* Maven: `mvnw` / `mvnw.cmd`

## Node.js & NPM/NPX
At the end of the course, there are two exercises involving Vue.js SPA clients.
If you want to perform these exercises, by building a Vue app using Vite,
you need to have Node.js installed as well.

* [Node.js & NPM/NPX](https://nodejs.org/en/)

N.B., it's possible to complete the exercises without Node.js, by just copy the
pre-built files. However, would not be as fun.

